# appointment-manager


Goal:
This application helps real estate employees to find out which houses and their customers.
keeps records of what it will show. Also between these appointment times, round trip
helps ensure that its processes are managed without overlapping each other.

Architectural
- Authentication
- Appointment creation screen => Address of the appointment, date and date of the customer to attend, surname, arrival of the appointment
locations
    > Start (the address where the real estate agent is located).
    > End location is selected from the map (address of the house to be displayed to the customer).
    > The distance between the start and end points is calculated (with mapquest api). After calculating the distance, what time the real estate agent should leave the office is calculated and likewise the time to return to the average office after the appointment (appointment time 1 hour).
- The page listing the appointments created. These appointments are saved by user, date and
can be filtered according to the calculated distance. When this list is first entered, the user
sees his appointments listed.

## Technologies
Back-end: Laravel Api
Front-end: VueJS
Database: MySql


## Project setup
```
npm install
```

Put your google maps api key in main.js.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
