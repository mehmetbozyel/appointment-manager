import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Appointments from '../views/Appointments.vue';
import Register from '../views/Register.vue';
import Login from '../views/Login.vue';
import Logout from '../views/Logout.vue';
import Chart from '../views/Chart.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/appointments',
    name: 'Appointments',
    component: Appointments,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      requiresVisitor: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      requiresVisitor: true,
    },
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
  },
  {
    path: '/chart',
    name: 'Chart',
    component: Chart,
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
