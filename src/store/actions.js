import axios from 'axios';

export default {
  deleteAppointment(context, id) {
    axios.delete(`/appointments/${id}`)
      .then(() => {
        context.commit('deleteAppointment', id);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getArrivetime(context, data) {
    axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;

    return new Promise((resolve, reject) => {
      axios.post('/apiPost', {
        lat: data.location.lat,
        lng: data.location.lng,
      })
        .then((response) => {
          context.commit('getArrivetime', response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
          console.log(error.response);
        });
    });
  },
  clearAppointments(context) {
    context.commit('clearAppointments');
  },
  register(context, data) {
    return new Promise((resolve, reject) => {
      axios.post('/register', {
        name: data.name,
        email: data.email,
        password: data.password,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  destroyToken(context) {
    axios.defaults.headers.common.Authorization = `Bearer ${context.state.token}`;

    return new Promise((resolve, reject) => {
      axios.post('/logout')
        .then((response) => {
          localStorage.removeItem('access_token');
          context.commit('destroyToken');
          resolve(response);
          // console.log(response);
          // context.commit('addTodo', response.data)
        })
        .catch((error) => {
          localStorage.removeItem('access_token');
          context.commit('destroyToken');
          reject(error);
        });
    });
  },

  retrieveToken(context, credentials) {
    return new Promise((resolve, reject) => {
      axios.post('/login', {
        username: credentials.username,
        password: credentials.password,
      })
        .then((response) => {
          // console.log(response.data.access_token);
          const token = response.data.access_token;

          localStorage.setItem('access_token', token);
          context.commit('retrieveToken', token);
          resolve(response);
          // context.commit('addAppointment', response.data)
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  },

  retrieveAppointments({ commit, state }) {
    axios.defaults.headers.common.Authorization = `Bearer ${state.token}`;

    axios.get('/appointments')
      .then((response) => {
        commit('retrieveAppointments', response.data);
      })
      .catch((error) => { console.log(error); });
  },

  addAppointment(context, appointment) {
    axios.post('/appointments', {
      customerName: appointment.customerName,
      time: appointment.time,
      suggestedOut: appointment.suggestedOut.toString(),
      estimatedReturn: appointment.estimatedReturn.toString(),
      distance: appointment.distance.toString(),
    })
      .then((response) => {
        context.commit('addAppointment', response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
