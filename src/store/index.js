import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import stateStore from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex);
axios.defaults.baseURL = 'http://localhost/api';
// axios.defaults.baseURL = 'https://api.mehmetbozyel.keenetic.pro/api';

export default new Vuex.Store({
  state: stateStore,
  getters,
  mutations,
  actions,
});
