export default {
  addAppointment(state, appointment) {
    state.appointments.push(appointment);
  },

  retrieveAppointments(state, appointments) {
    state.appointments = appointments;
  },

  retrieveToken(state, token) {
    state.token = token;
  },
  destroyToken(state) {
    state.token = null;
  },
  clearAppointments(state) {
    state.appointments = [];
  },
  getArrivetime(state, arrivetime) {
    state.arrivetime = arrivetime;
  },
  deleteAppointment(state, id) {
    const index = state.appointments.findIndex((item) => item.id === id);
    state.appointments.splice(index, 1);
  },
};
